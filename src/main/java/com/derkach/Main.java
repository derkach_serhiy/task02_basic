/*
 * Copyright Sun,Inc.
 */
package com.derkach;

/**
 * @author Serhoo Derkach
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        Numbers numbers = new Numbers();
        Fibonacci fibonacci = new Fibonacci();
        numbers.showOddEvenNumbers();
        fibonacci.start();
    }
}
