/*
 * Copyright Sun,Inc.
 */
package com.derkach;

import java.util.Scanner;

/**
 * @author Serhii Derkach
 */
class Numbers {
    /**
     * Scanning console.
     */
    private Scanner sc = new Scanner(System.in);

    /**
     * This method show us odd and even numbers.
     */
    void showOddEvenNumbers() {
        System.out.println("This program show odd and even "
                + "numbers from selected interval");
        System.out.println("Enter first number!");
        int first = sc.nextInt();
        System.out.println("Enter second number");
        int second = sc.nextInt();
        int sum = 0;
        int maxNumber = 0;
        for (int i = first; i < second; i++) {
            if (i % 2 != 0) {
                System.out.print(i + ", ");
                sum = sum + i;
            }
            maxNumber = i;

        }
        System.out.println();
        System.out.println("Max odd number " + maxNumber);
        System.out.println("Sum of odd numbers " + sum);
        sum = 0;
        for (int i = first; i <= second; i++) {
            if (i % 2 == 0) {
                System.out.print(i + ", ");
                sum = sum + i;
            }
            maxNumber = i;
        }
        System.out.println();
        System.out.println("Max even number " + maxNumber);
        System.out.println("Sum of even numbers " + sum);
    }
}
