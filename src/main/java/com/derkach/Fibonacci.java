/*
 * Copyright Sun,Inc.
 */
package com.derkach;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Serhii Derkach
 */
class Fibonacci {
    /**
     * Scanner.
     */
    private Scanner sc = new Scanner(System.in);
    /**
     * List.
     */
    private ArrayList list1 = new ArrayList();
    /**
     * List.
     */
    private ArrayList list2 = new ArrayList();

    /**
     * @throws InterruptedException because Thead.sleep need this
     */
    synchronized void start() throws InterruptedException {
        System.out.println();
        System.out.println("Fibonacci number!");
        System.out.println("Enter an integer, number which you want to see.");
        int m = sc.nextInt();
        System.out.println("Enter a delay which you want");
        int delay = sc.nextInt();
        for (int i = 0; i < m; i++) {
            System.out.print(fib(i) + " ");
            Thread.sleep(delay);
        }
        System.out.println();
        System.out.println("The biggest odd number "
                + list1.get(list1.size() - 1));
        System.out.println();
        System.out.println("The biggest even number "
                + list2.get(list2.size() - 1));
    }

    /**
     * @return result
     */
    private int fib(int i) {
        if (i <= 0) {
            return 0;
        }
        if (i <= 2) {
            return 1;
        }
        int prev = 1;
        int result = 1;
        for (int x = 2; x < i; x++) {
            result += prev;
            prev = result - prev;
        }
        if (result % 2 != 0) {
            list1.add(result);
        }
        if (result % 2 == 0) {
            list2.add(result);
        }
        return result;
    }
}
